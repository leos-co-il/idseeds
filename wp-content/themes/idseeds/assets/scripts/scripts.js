(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	function addToCartAnimation(button){
		var target        = $('#mini-cart'),
			target_offset = target.offset();

		var target_x = target_offset.left,
			target_y = target_offset.top;

		var obj_id = 1 + Math.floor(Math.random() * 100000),
			obj_class = 'cart-animation-helper',
			obj_class_id = obj_class + '_' + obj_id;

		var obj = $("<div>", {
			'class': obj_class + ' ' + obj_class_id
		});

		button.parent().parent().append(obj);

		var obj_offset = obj.offset(),
			dist_x = target_x - obj_offset.left + 10,
			dist_y = target_y - obj_offset.top + 10,
			delay  = 0.8; // seconds

		setTimeout(function(){
			obj.css({
				'transition': 'transform ' + delay + 's ease-in',
				'transform' : "translateX(" + dist_x + "px)"
			});
			$('<style>.' + obj_class_id + ':after{ \
				transform: translateY(' + dist_y + 'px); \
				opacity: 1; \
				z-index: 99999999999; \
				border-radius: 100%; \
				height: 20px; \
				width: 20px; margin: 0; \
			}</style>').appendTo('head');
		}, 0);


		obj.show(1).delay((delay + 0.02) * 1000).hide(1, function() {
			$(obj).remove();
		});
	}
	function updateMiniCart(key, qty){

		var cartEl = $('.mini-cart-wrap');

		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'POST',
			data: {
				action: 'update_mini_cart',
				id: key,
				qty: qty
			},
			success: function (results) {
				cartEl.html(results);
				update_cart_count();
				$('.preloader').removeClass('now-loading');
			}
		});
	}
	function update_cart_count(){
		var count = $('#cart-count');
		count.html('<i class="far fa-smile-beam fa-spin" style="color: #fff"></i>');

		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'POST',
			data: {
				action: 'update_cart_count',
			},
			success: function (results) {
				count.html(results/10);
			}
		});

	}
	$('#mini-cart').click(function (e){
		e.preventDefault();

		$('#mini-cart-el').addClass('show-cart');
	})

	$(document).on('click', '#close-cart', function (){
		$('#mini-cart-el').removeClass('show-cart');
	})
	$('.product-small-card .single_add_to_cart_button').click(function (e) {
		e.preventDefault();
		addToCartAnimation($(this));
		var cartEl = $('.mini-cart-wrap');
		cartEl.html('<div class="cart-loading"><i class="fas fa-spinner fa-pulse"></i></div>');


		var btn = $(this);
		btn.addClass('loading');


		$(this).addClass('adding-cart');
		var product_id = $(this).data('id');
		var quantity = $('.qty-for-' + product_id).val();


		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'POST',
			data: {
				action: 'add_product_to_cart',
				product_id: product_id,
				quantity: quantity,
			},

			success: function (results) {
				btn.removeClass('loading').addClass('clicked');
				cartEl.html(results);
				update_cart_count();
			}
		});

	});
	$('body').on('click', '.remove_from_cart_x', function (e) {
		e.preventDefault();
		var productID = $(this).data('key');
		var cartEl = $('.mini-cart-wrap');
		cartEl.html('<div class="cart-loading"><i class="fas fa-spinner fa-pulse"></i></div>');
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'POST',
			data: {
				action: 'remove_item_from_cart',
				product_id: productID,
			},

			success: function (results) {
				update_cart_count()
				cartEl.html(results);
			}
		});
	});

	$(document).on('click', '.plus, .minus', function () {

		var $_class = $(this).hasClass('mini-cart-ctrl') ? '.mini-cart-qty-for-' : '.qty-for-';
		var qty =  $($_class + $(this).data('id'));
		var val = parseFloat(qty.val());
		var max = 9999;
		var min = 1;
		var step = 1;

		if ($(this).is('.plus')) {
			if (max && (max <= val)) {
				qty.val(max);
			} else {
				qty.val(val + step);
			}
		} else {
			if (min && (min >= val)) {
				qty.val(min);
			} else if (val > 1) {
				qty.val(val - step);
			}
		}

		if($(this).hasClass('mini-cart-ctrl')){
			$('.preloader').addClass('now-loading');
			updateMiniCart($(this).data('key'), qty.val())
		}

	});
	$( document ).ready(function() {
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
		$('.cat-item-img').hover(function (){
			$(this).children('.cat-item-info').children('.cat-item-info-part').slideFadeToggle();
		});
		$('.search-trigger').click(function () {
			$('.pop-search').addClass('show-search');
			$('.float-search').addClass('show-float-search');
		});
		$('.close-search').click(function () {
			$('.pop-search').removeClass('show-search');
			$('.float-search').removeClass('show-float-search');
		});
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
		});
		$('.main-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: true,
		});
		$('.reviews-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$('.cat-slider').slick({
			slidesToShow: 9,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1800,
					settings: {
						slidesToShow: 8,
					}
				},
				{
					breakpoint: 1600,
					settings: {
						slidesToShow: 7,
					}
				},
				{
					breakpoint: 1400,
					settings: {
						slidesToShow: 5,
					}
				},
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 2,
					}
				},
			]
		});
		$('.product-slider-related').slick({
			slidesToShow: 6,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1800,
					settings: {
						slidesToShow: 5,
					}
				},
				{
					breakpoint: 1600,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$('.gallery-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			fade: true,
			asNavFor: '.thumbs'
		});
		$('.thumbs').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			asNavFor: '.gallery-slider',
			dots: false,
			arrows: true,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				// {
				// 	breakpoint: 768,
				// 	settings: {
				// 		slidesToShow: 2,
				// 	}
				// },
			]
		});
		$('.product-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		$('.play-button-post').click(function() {
			var id = $(this).data('id');
			var iFrame = '<iframe src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('.put-video-here').addClass('show').html(iFrame);
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').children('.faq-arrow').addClass('arrow-top');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').children('.faq-arrow').removeClass('arrow-top');
		});
		var prodAcc = $('#accordion-product');
		prodAcc.on('shown.bs.collapse', function (e) {
			$('#accordion-product .collapse.show').parent().addClass('active');
		});

		prodAcc.on('hide.bs.collapse', function (e) {
			$('#accordion-product .collapse.show').parent().removeClass('active');
		});
		//Socials
		$('.trigger-wrap').hover(function(){
			$(this).children('.all-socials').addClass('show-share');
		}, function(){
			$(this).children('.all-socials').removeClass('show-share');
		});
	});

	//More products
	var button = $( '#loadmore a' );
	var paged = button.data( 'paged' );
	var	maxPages = button.data( 'maxpages' );
	var textLoad = button.data('loading');
	var textLoadMore = button.data('load');

	button.click( function( event ) {

		event.preventDefault();
		var ids = '';
		$('.more-prod').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		$.ajax({
			type : 'POST',
			dataType: 'json',
			url: '/wp-admin/admin-ajax.php',
			data : {
				paged : paged,
				ids: ids,
				action : 'loadmore'
			},
			beforeSend : function() {
				button.text(textLoad);
			},
			success : function( data ){

				paged++;
				$('.put-here-prods').append(data.html);
				button.text(textLoadMore);

				if( paged === maxPages ) {
					button.remove();
				}
				if (!data.html) {
					$('.more-link').addClass('hide');
				}
			}

		});

	} );
	$(document).on('added_to_wishlist removed_from_wishlist', function () {
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'post',
			dataType: "json",
			data: {
				action: 'yith_wcwl_update_wishlist_count',
			},
			success: function (data) {
				$('#yith-w-count').html(data.count);
			}
		});
	});
	$('.load-more-posts').click(function(e) {
		e.preventDefault();
		var btn = $(this);
		btn.addClass('loading');
		btn.append('<div class="cart-loading mx-2"><i class="fas fa-spinner fa-pulse"></i></div>');
		var termID = $(this).data('term');
		// var params = $('.take-json').html();
		var ids = '';
		var type = $(this).data('type');
		var count = $(this).data('count');
		var page = $(this).data('page');
		var quantity = $('.more-card').length;

		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				termID: termID,
				ids: ids,
				count: count,
				quantity : quantity,
				type: type,
				page: page,
				action: 'get_more_function',
			},
			success: function (data) {
				btn.removeClass('loading');
				$('.cart-loading').remove();
				if (!data.html || data.quantity) {
					btn.addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});

})( jQuery );
