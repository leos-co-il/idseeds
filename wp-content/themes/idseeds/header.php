<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
	<div class="header-top">
		<div class="container-fluid">
			<div class="row align-items-center justify-content-between">
				<div class="col-sm-auto col-3 header-logo-col">
					<?php if ($logo = opt('logo')) : ?>
						<a href="/" class="logo">
							<img src="<?= $logo['url'] ?>" alt="logo">
						</a>
					<?php endif; ?>
				</div>
				<?php if ($offer = opt('offer')) : ?>
					<div class="col d-flex justify-content-lg-center justify-content-end pr-md-0 pr-5 d-col-none-m">
						<h3 class="header-offer"><?= $offer; ?></h3>
					</div>
				<?php endif; ?>
				<div class="col-lg-auto col-md-12 col d-flex justify-content-lg-end justify-content-sm-between justify-content-end align-items-center all-header-col">
					<div class="search-form-wrapper">
						<?php get_search_form(); ?>
					</div>
					<div class="d-flex align-items-center">
						<?php if (get_lang()) : ?>
							<div class="langs-wrap">
								<?php do_action( 'wpml_footer_language_selector'); ?>
							</div>
						<?php endif; ?>
						<div class="header-btn" id="mini-cart">
							<img src="<?= ICONS ?>basket.png" alt="shopping-cart">
							<span class="count-circle" id="cart-count">
							<?= WC()->cart->get_cart_contents_count(); ?>
						</span>
						</div>
						<?php if(defined( 'YITH_WCWL' )): ?>
							<a href="<?= get_permalink(get_option('yith_wcwl_wishlist_page_id')); ?>" class="header-btn">
								<img src="<?= ICONS ?>wishlist.png" alt="wishlist">
							</a>
						<?php endif; ?>
						<div class="registers-wrapper mx-2">
							<img src="<?= ICONS ?>user.png">
							<a href="<?= get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" class="login-link-header">
								<?= lang_text(['en' => 'Log in', 'es' => 'Iniciar sesión', 'ja' => 'ログイン'], 'en'); ?>
							</a>
							<span class="login-link-header">|</span>
							<a href="<?= wp_registration_url(); ?>" class="register-link">
								<?= lang_text(['en' => 'Register', 'es' => 'Registrarse', 'ja' => '登録'], 'en'); ?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-auto">
					<nav id="MainNav" class="h-100">
						<div id="MobNavBtn">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<?php getMenu('header-menu', '1', '', 'main_menu h-100'); ?>
					</nav>
				</div>
			</div>
		</div>
	</div>
</header>
<div class="mini-cart-wrap" id="mini-cart-el">
	<?php wc_get_template( 'cart/mini-cart.php' ); ?>
</div>

<div class="triggers-col">
	<?php if ($whatsapp = opt('whatsapp')) : ?>
		<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="social-link">
			<img src="<?= ICONS ?>whatsapp.png" alt="whatsapp-icon">
		</a>
	<?php endif; ?>
	<?php if ($facebook = opt('facebook')) : ?>
		<a href="<?= $facebook; ?>" class="social-link">
			<img src="<?= ICONS ?>facebook.png" alt="facebook-icon">
		</a>
	<?php endif; ?>
	<?php if ($instagram = opt('instagram')) : ?>
		<a href="<?= $instagram; ?>" class="social-link">
			<img src="<?= ICONS ?>instagram.png" alt="instagram-icon">
		</a>
	<?php endif;
	if ($tiktok = opt('tiktok')) : ?>
		<a href="<?= $tiktok; ?>" class="social-link">
			<img src="<?= ICONS ?>tiktok.png" alt="tiktok-icon">
		</a>
	<?php endif;
	if ($mail = opt('mail')) : ?>
		<a href="mailto:<?= $mail; ?>" class="social-link">
			<img src="<?= ICONS ?>mail.png" alt="mail-icon">
		</a>
	<?php endif; ?>
</div>
