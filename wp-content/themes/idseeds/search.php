<?php
get_header();
$fields = get_fields();
?>
<div class="post-output-block page-body pb-5">
	<div class="container pt-5">
		<?php
		$s = get_search_query();
		$args_1 = array(
			'post_type' => 'post',
			'suppress_filters' => false,
			's' => $s
		);
		$args_2 = array(
			'post_type' => 'product',
			'suppress_filters' => false,
			's' => $s
		);
		$the_query_1 = new WP_Query( $args_1 );
		$the_query_2 = new WP_Query( $args_2 );
		if ( $the_query_1->have_posts() ) { ?>
		<h4 class="base-title my-3"><?= esc_html__('תוצאות חיפוש עבור:','leos');?><?= get_query_var('s') ?></h4>
		<div class="row justify-content-center align-items-stretch">
			<?php while ( $the_query_1->have_posts() ) { $the_query_1->the_post();
				$link = get_the_permalink(); ?>
				<div class="col-xl-3 col-lg-4 col-sm-6 col-12 post-col post-col-base more-card" data-id="<?= $args['post']->ID; ?>">
					<div class="post-item">
						<a class="post-item-image" href="<?= $link; ?>"
							<?php if (has_post_thumbnail()) : ?>
								style="background-image: url('<?= postThumb(); ?>')"
							<?php endif;?>>
						</a>
						<div class="post-item-content">
							<h2 class="post-item-title"><?php the_title(); ?></h2>
							<p class="post-item-text">
								<?= text_preview(get_the_content(), 10); ?>
							</p>
						</div>
						<a href="<?= $link; ?>" class="post-link">
							<?= lang_text(['ja' => '続きを読む', 'es' => 'Lee mas', 'en' => 'Read more'], 'en'); ?>
						</a>
					</div>
				</div>
			<?php }
			} ?>
		</div>
		<div class="row justify-content-center align-items-stretch">
			<?php if ( $the_query_2->have_posts() ) {
				while ( $the_query_2->have_posts() ) { $the_query_2->the_post(); ?>
					<div class="col-lg-3 col-md-6 col-sm-10 col-12 mb-4">
						<?php
						$post_object = get_post( get_the_ID());

						setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

						wc_get_template_part( 'content', 'product' );
						?>
					</div>
				<?php }
			} ?>
		</div>
		<?php if (!$the_query_1->have_posts() && !$the_query_2->have_posts()) : ?>
			<div class="col-12 pt-5">
				<h4 class="base-title">
					<?= esc_html__('שום דבר לא נמצא','leos'); ?>
				</h4>
			</div>
			<div class="alert alert-info text-center mt-5">
				<p><?= esc_html__('מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.','leos'); ?></p>
			</div>
		<?php endif; ?>
	</div>
</div>
</div>
</div>
<?php get_footer(); ?>
