<?php

$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
?>

<footer>
	<a id="go-top">
		<img src="<?= ICONS ?>to-top.png" alt="arrow" class="arrow-top">
		<h3 class="to-top-text">
			<?= lang_text(['en' => 'back up', 'es' => 'hasta arriba', 'ja' => 'トップに'], 'en'); ?>
		</h3>
	</a>
	<div class="footer-form">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-9 col-lg-10 col-md-11 col-12">
					<div class="form-col">
						<div class="row mb-4 justify-content-center align-items-center">
							<?php if ($f_title = opt('foo_form_title')) : ?>
								<div class="col-auto">
									<h2 class="form-title"><?= $f_title; ?></h2>
								</div>
							<?php endif;
							if ($f_text = opt('foo_form_subtitle')) : ?>
								<div class="col-auto">
									<h3 class="form-subtitle"><?= $f_text; ?></h3>
								</div>
							<?php endif; ?>
						</div>
						<?php getForm('61'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-main">
		<div class="container-fluid">
			<div class="row justify-content-xl-start justify-content-center">
				<div class="col-11 foo-main-col">
					<div class="row justify-content-between">
						<div class="col-xl-auto col-lg-4 col-sm-6 foo-menu foo-socials-menu">
							<h3 class="foo-title">
								<?= lang_text(['en' => 'follow us on social media', 'es' => 'Síguenos en las redes sociales', 'ja' => 'ソーシャルメディアでフォローしてください'], 'en'); ?>
							</h3>
							<div class="menu-border-top social-menu-foo">
								<?php if ($whatsapp = opt('whatsapp')) : ?>
									<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="social-link">
										<img src="<?= ICONS ?>whatsapp.png" alt="whatsapp-icon">
									</a>
								<?php endif; ?>
								<?php if ($facebook = opt('facebook')) : ?>
									<a href="<?= $facebook; ?>" class="social-link">
										<img src="<?= ICONS ?>facebook.png" alt="facebook-icon">
									</a>
								<?php endif; ?>
								<?php if ($instagram = opt('instagram')) : ?>
									<a href="<?= $instagram; ?>" class="social-link">
										<img src="<?= ICONS ?>instagram.png" alt="instagram-icon">
									</a>
								<?php endif;
								if ($tiktok = opt('tiktok')) : ?>
									<a href="<?= $tiktok; ?>" class="social-link">
										<img src="<?= ICONS ?>tiktok.png" alt="tiktok-icon">
									</a>
								<?php endif; ?>
							</div>
						</div>
						<div class="col-xl col-12 col-lg-4 col-sm-6 foo-menu contacts-footer-menu">
							<h3 class="foo-title">
								<?= lang_text(['en' => 'Contact us', 'es' => 'Contacta con nosotras', 'ja' => 'お問い合わせ'], 'en'); ?>
							</h3>
							<div class="menu-border-top contact-menu-foo">
								<ul class="contact-list d-flex flex-column">
									<?php if ($tel) : ?>
										<li>
											<a href="tel:<?= $tel; ?>" class="contact-info-footer">
												<span><?= lang_text(['en'=> 'tel:', 'es' => 'tel:', 'ja' => '電話：'],'en').$tel; ?></span>
											</a>
										</li>
									<?php endif;
									if ($address) : ?>
										<li>
											<a href="https://waze.com/ul?q=<?= $address; ?>"
											   class="contact-info-footer" target="_blank">
												<span><?= lang_text(['en'=> 'address:', 'es' => 'dirección:', 'ja' => '住所：'],'en').$address; ?></span>
											</a>
										</li>
									<?php endif;
									if ($mail) : ?>
										<li>
											<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
												<span><?= lang_text(['en'=> 'mail service:', 'es' => 'servicio de correo:', 'ja' => 'メールサービス：'],'en').$mail; ?></span>
											</a>
										</li>
									<?php endif; ?>
								</ul>
							</div>
						</div>
						<div class="col-xl col-12 foo-menu col-sm-6 links-footer-menu">
							<div class="menu-border-top">
								<?php getMenu('footer-links-menu', '1', 'hop-hey two-columns'); ?>
							</div>
						</div>
						<div class="col-xl-auto col-lg-4 col-sm-6 col-12 foo-menu main-footer-menu">
							<h3 class="foo-title">
								<?= lang_text(['en' => 'web map', 'es' => 'mapa web:', 'ja' => 'Webマップ'], 'en'); ?>
							</h3>
							<div class="menu-border-top">
								<?php getMenu('footer-menu', '1', 'hop-hey'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($payments = opt('payments')) : ?>
		<div class="payments-line">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-11 payment-col">
						<?php foreach ($payments as $payment) : ?>
							<img src="<?= $payment['url']; ?>" alt="payment-system">
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
