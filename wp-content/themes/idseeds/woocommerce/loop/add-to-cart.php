<?php
/**
 * Loop Add to Cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/add-to-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;


?>

<div class="add-to-cart-wrap d-flex">

	<div class="qty-wrap">
		<div class="minus" data-id="<?= $product->get_id() ?>"> - </div>
		<?php

		do_action( 'woocommerce_before_add_to_cart_quantity' );

		woocommerce_quantity_input(
			array(
				'classes' => 'qty-for-' .  $product->get_id(),
				'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
				'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
				'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
			)
		);

		do_action( 'woocommerce_after_add_to_cart_quantity' );
		?>
		<div class="plus" data-id="<?= $product->get_id() ?>"> + </div>
	</div>

	<div class="btn-wrap">
		<button type="submit" name="add-to-cart" data-id="<?= $product->get_id() ?>" value="<?php echo esc_attr( $product->get_id() ); ?>"
				class="single_add_to_cart_button button alt">
			<span>
				<?= lang_text(['en' => 'add to cart', 'es' => 'Añadir al carrito', 'ja' => 'カートに追加'], 'en'); ?>
			</span>
			<img src="<?= ICONS ?>basket.png" alt="add-to-cart">
		</button>
		<a href="<?= get_the_permalink($product); ?>" class="single-product-show-link">
			<?= lang_text(['en' => 'details', 'es' => 'detalles', 'ja' => '詳細'], 'en'); ?>
		</a>
	</div>
</div>

