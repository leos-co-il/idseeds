<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

$in_wishlist = null;

if ( function_exists( 'YITH_WCWL' ) ) {
	$in_wishlist = YITH_WCWL()->is_product_in_wishlist( $product->get_id() );
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

if ( $product->is_in_stock() ) : ?>


	<div class="add-to-cart-single-item d-flex">
		<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

		<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
			<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
			<div class="qty-wrap">
				<div class="minus" data-id="<?= $product->get_id() ?>"> - </div>
				<?php

				do_action( 'woocommerce_before_add_to_cart_quantity' );

				woocommerce_quantity_input(
						array(
								'classes' => 'qty-for-' .  $product->get_id(),
								'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
								'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
								'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
						)
				);

				do_action( 'woocommerce_after_add_to_cart_quantity' );

				?>
				<div class="plus" data-id="<?= $product->get_id() ?>"> + </div>
			</div>
			<div class="d-flex align-items-stretch justify-content-start buttons-product-wrapper">
				<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="add-custom single_add_to_cart_button button alt">
				<span>
					<?= lang_text(['en' => 'add to cart', 'es' => 'Añadir al carrito', 'ja' => 'カートに追加'], 'en'); ?>
				</span>
					<img src="<?= ICONS ?>basket-white.png" alt="add-to-cart" class="mx-2">
				</button>
				<div class="wrapper-product-wish">
					<?php if($in_wishlist !== null): ?>
						<div class="wishlist-btn">
							<?php if($in_wishlist): ?>
								<span class="item-wishlist has-tooltip">
							  <span class="tooltiptext">
								  <?= lang_text(['en' => 'Product is already in wishlist', 'es' => 'El producto ya está en la lista de deseos', 'ja' => '製品はすでにウィッシュリストにあります'], 'en'); ?>
							  </span>
								<?= svg_simple(THEMEPATH . '/assets/images/like_full.svg') ?>
							</span>
							<?php elseif ($in_wishlist === false): ?>
								<?= do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
		</form>

		<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
	</div>

<?php endif; ?>
