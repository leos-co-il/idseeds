<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 5.2.0
 */

defined( 'ABSPATH' ) || exit;
$logo = opt('mini-cart-logo');
do_action( 'woocommerce_before_mini_cart' ); ?>
<div class="preloader">
	<?= svg_simple(THEMEPATH . '/assets/images/loading.svg') ?>
</div>
<div class="cart-header">
	<button id="close-cart">
		<img src="<?= ICONS ?>close.png" alt="close-cart">
	</button>
	<div class="mini-cart-logo">
		<?= get_img($logo, 'large', '', '', 'images') ?>
	</div>
</div>

<div class="user-hello">
	<?php if(is_user_logged_in()):

		$user_name = get_user_meta( get_current_user_id(), 'first_name', true );

		?>
		<h6>
			<span><?= lang_text(['en' => 'Hello!', 'es' => 'Hola!', 'ja' => 'こんにちは'], 'en'); ?></span>
			<span><?= $user_name ? $user_name . '!' : lang_text(['en' => 'Happy customer!', 'es' => 'Cliente feliz!', 'ja' => '幸せな顧客！'], 'en'); ?></span>
		</h6>
	<?php else: ?>
		<a href="<?= get_permalink( wc_get_page_id( 'myaccount' ) ) ?>" class="login-link">
			<span>התחבר | הירשם</span>
		</a>
	<?php endif; ?>
</div>

<div class="my-bag">
	MY CART (<?= WC()->cart->get_cart_contents_count() ?>)
</div>

<?php if ( ! WC()->cart->is_empty() ) : ?>
	<ul class="woocommerce-mini-cart cart_list product_list_widget <?php echo esc_attr( $args['list_class'] ); ?>">
		<?php
		do_action( 'woocommerce_before_mini_cart_contents' );

		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				$product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );
				$thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
				$product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
				$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
				$thumb_id = get_post_thumbnail_id( $product_id )
				?>
				<li class="woocommerce-mini-cart-item <?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>">
					<?php
					echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
							'woocommerce_cart_item_remove_link',
							sprintf(
									'<a href="%s" class="remove remove_from_cart_button remove_from_cart_x" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s">&times;</a>',
									esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
									esc_attr__( 'Remove this item', 'woocommerce' ),
									esc_attr( $product_id ),
									esc_attr( $cart_item_key ),
									esc_attr( $_product->get_sku() )
							),
							$cart_item_key
					);
					?>
					<?php if ( empty( $product_permalink ) ) : ?>
						<?php echo $thumbnail . wp_kses_post( $product_name ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
					<?php else : ?>
						<div class="mini__prod-thumb">
							<?= get_img(['ID' => $thumb_id], 'medium', false, '', 'images') ?>
						</div>
						<div class="mini__prod-info">
							<p class="title">
								<a href="<?= $product_permalink ?>">
									<?= $product_name ?>
								</a>
							</p>
							<div class="mini__price">
								<?php
								$regular_price = $_product->get_regular_price();
								$sale_price = $_product->is_on_sale() ? $_product->get_sale_price() : null;
								$currency_symbol = get_woocommerce_currency_symbol();
								if ($_product->is_type( 'simple' )) {
									$sale_price     =  $_product->get_sale_price();
									$regular_price  =  $_product->get_regular_price();
								}
								elseif($_product->is_type('variable')){
									$sale_price     =  $_product->get_variation_sale_price( 'min', true );
									$regular_price  =  $_product->get_variation_regular_price( 'max', true );
								}
								?>
								<span class="price d-flex align-items-center">
										<?php if($sale_price): ?>
											<del>
												<span class="woocommerce-Price-amount amount">
													<bdi>
														<span class="woocommerce-Price-currencySymbol"><?= $currency_symbol ?></span>
														<?= $regular_price ?>
													</bdi>
												</span>
											</del>
										<?php endif; ?>
									<ins>
										<span class="woocommerce-Price-amount amount mr-2">
											<bdi>
												<span class="woocommerce-Price-currencySymbol"><?= $currency_symbol ?></span>
												<?= $sale_price ? $sale_price : $regular_price ?>
											</bdi>
										</span>
									</ins>
								</span>
							</div>
							<div class="qty-wrap">
								<div class="plus mini-cart-ctrl" data-key="<?= $cart_item_key ?>" data-id="<?= $_product->get_id() ?>"> + </div>
								<?php

								do_action( 'woocommerce_before_add_to_cart_quantity' );

								woocommerce_quantity_input(
										array(
												'classes' => 'mini-cart-qty-for-' .  $_product->get_id(),
												'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $_product->get_min_purchase_quantity(), $_product ),
												'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $_product->get_max_purchase_quantity(), $_product ),
												'input_value' => $cart_item['quantity'], // WPCS: CSRF ok, input var ok.
										)
								);

								do_action( 'woocommerce_after_add_to_cart_quantity' );

								?>
								<div class="minus mini-cart-ctrl" data-key="<?= $cart_item_key ?>" data-id="<?= $_product->get_id() ?>"> - </div>
							</div>
						</div>
					<?php endif; ?>
					<?php echo wc_get_formatted_cart_item_data( $cart_item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
					<!--					--><?php //echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $product_price ) . '</span>', $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
				</li>
				<?php
			}
		}
		do_action( 'woocommerce_mini_cart_contents' );
		?>
	</ul>

	<div class="mini-cart-bottom">
		<p class="woocommerce-mini-cart__total total">
			<?php
			/**
			 * Hook: woocommerce_widget_shopping_cart_total.
			 *
			 * @hooked woocommerce_widget_shopping_cart_subtotal - 10
			 */
			do_action( 'woocommerce_widget_shopping_cart_total' );
			?>
		</p>
		<?php do_action( 'woocommerce_widget_shopping_cart_before_buttons' ); ?>

		<p class="woocommerce-mini-cart__buttons buttons centered">
			<a href="<?= wc_get_cart_url() ?>">
				<?= lang_text(['en' => 'shopping bag', 'es' => 'bolsa de la compra', 'ja' => 'ショッピングバッグ'], 'en'); ?>
			</a>
		</p>

		<?php do_action( 'woocommerce_widget_shopping_cart_after_buttons' ); ?>
	</div>


<?php else : ?>

	<p class="woocommerce-mini-cart__empty-message"><?php esc_html_e( 'No products in the cart.', 'woocommerce' ); ?></p>

<?php endif; ?>

<?php do_action( 'woocommerce_after_mini_cart' ); ?>
