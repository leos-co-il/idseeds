<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$post_id = $product->get_id();
$fields = get_fields();
$category = get_the_terms($post_id, 'product_cat');
$ids = [];
if ($category) {
	foreach ($category as $cat_id) {
		$ids[] = $cat_id->term_id;
	}
}
$post_link = get_the_permalink();
$product_thumb = wp_get_attachment_image_url( get_post_thumbnail_id($post_id), 'full' );;
$product_gallery = $product->get_gallery_image_ids();
$product_gallery_images = [];
if($product_gallery){
	foreach ($product_gallery as $_item){
		$product_gallery_images[] = wp_get_attachment_image_url($_item, 'large', '');
	}
}
$info_content = get_the_content();
$related_products = $fields['related_products'] ? $fields['related_products'] : get_posts([
		'post_type' => 'product',
		'exclude' => $post_id,
		'numberposts' => 4,
		'tax_query' => [
				[
						'taxonomy' => 'product_cat',
						'field' => 'term_id',
						'terms' => $ids,
				],
		],
]);
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'page-body', $product ); ?>>
	<div class="container-fluid product-body">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-11">
				<div class="row justify-content-between">
					<div class="col-12 hidden-title">
						<h2 class="product-title"><?php the_title(); ?></h2>
					</div>
					<div class="col-lg-6 d-flex flex-column">
						<div class="summary entry-summary mb-0">
							<?php
							/**
							 * Hook: woocommerce_single_product_summary.
							 *
							 * @hooked woocommerce_template_single_title - 5
							 * @hooked woocommerce_template_single_rating - 10
							 * @hooked woocommerce_template_single_price - 10
							 * @hooked woocommerce_template_single_excerpt - 20
							 * @hooked woocommerce_template_single_add_to_cart - 30
							 * @hooked woocommerce_template_single_meta - 40
							 * @hooked woocommerce_template_single_sharing - 50
							 * @hooked WC_Structured_Data::generate_product_data() - 60
							 */
							do_action( 'woocommerce_single_product_summary' ); ?>
							<div class="base-output mb-3">
								<?php do_action('display_excerpt_custom'); ?>
							</div>
							<?php if ($fields['degree_of_difficulty']) : ?>
								<div class="product-icons-wrapper">
									<h2 class="base-title">
										<?= lang_text(['en' => 'Degree of difficulty:', 'es' => 'Grado de dificultad:', 'ja' => '難易度:'], 'en'); ?>
									</h2>
									<div class="icons-line">
										<?php foreach ($fields['degree_of_difficulty'] as $item) : ?>
											<img src="<?= $item['url']; ?>" alt="icon-difficulty" class="prod-icon">
										<?php endforeach; ?>
									</div>
								</div>
							<?php endif;
							do_action('woocommerce_single_product_price');
							do_action( 'woocommerce_single_product_cart' ); ?>
						</div>
						<div class="socials-share">
					<span class="share-text">
						 <?= lang_text(['es' => 'cuota', 'en' => 'share', 'ja' => '共有'], 'en'); ?>
					</span>
							<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
							   class="social-share-link wow fadeInUp" data-wow-delay="0.6s">
								<img src="<?= ICONS ?>share-mail.png">
							</a>
							<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>"
							   class="social-share-link wow fadeInUp" data-wow-delay="0.4s">
								<img src="<?= ICONS ?>share-whatsapp.png">
							</a>
							<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
							   class="social-share-link wow fadeInUp" data-wow-delay="0.2s">
								<img src="<?= ICONS ?>share-facebook.png">
							</a>
						</div>
					</div>
					<div class="col-xl-5 col-lg-6 gallery-slider-wrap arrows-slider">
						<div class="gallery-slider">
							<?php if($product_thumb): ?>
								<div class="p-2">
									<a class="big-slider-item" style="background-image: url('<?= $product_thumb; ?>')"
									   href="<?= $product_thumb; ?>" data-lightbox="images"></a>
								</div>
							<?php endif;
							foreach ($product_gallery_images as $img): ?>
								<div class="p-2">
									<a class="big-slider-item" style="background-image: url('<?= $img; ?>')"
									   href="<?= $img; ?>" data-lightbox="images">
									</a>
								</div>
							<?php endforeach; ?>
						</div>
						<div class="thumbs">
							<?php if($product_thumb): ?>
								<div class="p-2">
									<a class="thumb-item" style="background-image: url('<?= $product_thumb; ?>')"
									   href="<?= $product_thumb; ?>" data-lightbox="images-small"></a>
								</div>
							<?php endif;
							foreach ($product_gallery_images as $img): ?>
								<div class="p-2">
									<a class="thumb-item" style="background-image: url('<?= $img; ?>')"
									   href="<?= $img; ?>" data-lightbox="images-small">
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col">
				<?php
				/**
				 * Hook: woocommerce_after_single_product_summary.
				 *
				 * @hooked woocommerce_output_product_data_tabs - 10
				 * @hooked woocommerce_upsell_display - 15
				 * @hooked woocommerce_output_related_products - 20
				 */
				do_action( 'woocommerce_after_single_product_summary' );
				?>
			</div>
		</div>
	</div>
</div>
<?php if ($info_content) : ?>
	<section class="product-more-content mt-5">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-12">
					<div class="row">
						<div class="col-12">
							<div class="growing-title-wrap">
								<h2 class="product-title">
									<?= lang_text(['en' => 'Growing conditions', 'es' => 'Condiciones de crecimiento', 'ja' => '成長条件'], 'en'); ?>
								</h2>
							</div>
						</div>
						<div class="col-12">
							<div class="growing-content-wrap">
								<div class="base-output">
									<?= $info_content; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($related_products) : ?>
	<div class="products-output mb-5">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-auto mb-4">
					<h2 class="base-title">
						<?= lang_text(['en' => 'you may also like', 'es' => 'También te puede interesar', 'ja' => 'あなたも好きかも'], 'en'); ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<div class="col-xl-9 col-lg-10 col-11">
					<?php get_template_part('views/partials/related', 'slider', [
							'products' => $related_products,
					]); ?>
				</div>
			</div>
		</div>
	</div>
<?php endif;
do_action( 'woocommerce_after_single_product' ); ?>
