<?php
$search = lang_text(['en' => 'search', 'es' => 'buscar', 'ja' => '探す'], 'en');
?>
<form role="search" method="get" class="search-form" action="">
	<input id="search-input" type="search" class="search-input" placeholder="<?= $search; ?>" value="" name="s" title="<?= $search; ?>" />
	<input type="submit" class="search-submit" value="" />
</form>
