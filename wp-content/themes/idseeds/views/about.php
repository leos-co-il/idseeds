<?php
/*
Template Name: About
*/

get_header();
$fields = get_fields();

?>

<article class="home-about-block">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-between align-items-center">
			<div class="<?= has_post_thumbnail() ? 'col-lg-7 col-12' : 'col-12'; ?> d-flex flex-column justify-content-start align-items-center">
				<div class="base-output mb-3">
					<h1 class="base-title"><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
			<?php if (has_post_thumbnail()) : ?>
				<div class="col-xl-4 col-lg-5 mt-lg-0 mt-4">
					<img src="<?= postThumb(); ?>" alt="about-image" class="home-about-img">
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php if ($fields['about_benefits']) {
	get_template_part('views/partials/repeat', 'benefits', [
			'benefits' => $fields['about_benefits'],
	]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
