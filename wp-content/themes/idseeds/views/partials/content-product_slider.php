<?php

if(!isset($args['products']))
	return;

?>

<div class="product-slider arrows-slider">
	<?php
	$slider_item = array_chunk($args['products'], 4);
	foreach ($slider_item as $_p){
		echo '<div class="slide-item p-2"><div class="product-slider-wrapper">';
		foreach ($_p as $post) {
			setup_postdata($GLOBALS['post'] =& $post);
			wc_get_template_part( 'content', 'product' );
		}
		echo '</div></div>';
	}
	wp_reset_postdata();

	?>
</div>
