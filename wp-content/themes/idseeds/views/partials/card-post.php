<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-xl-3 col-lg-4 col-sm-6 col-12 post-col post-col-base more-card" data-id="<?= $args['post']->ID; ?>">
		<div class="post-item">
			<a class="post-item-image" href="<?= $link; ?>"
				<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')"
				<?php endif;?>>
			</a>
			<div class="post-item-content">
				<h2 class="post-item-title"><?= $args['post']->post_title; ?></h2>
				<p class="post-item-text">
					<?= text_preview($args['post']->post_content, 10); ?>
				</p>
			</div>
			<a href="<?= $link; ?>" class="post-link">
				<?= lang_text(['ja' => '続きを読む', 'es' => 'Lee mas', 'en' => 'Read more'], 'en'); ?>
			</a>
		</div>
	</div>
<?php endif; ?>
