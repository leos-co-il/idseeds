<?php if (isset($args['benefits']) && $args['benefits']) : ?>
	<section class="benefits-block">
		<div class="container">
			<div class="row justify-content-center align-items-start">
				<?php foreach ($args['benefits'] as $key => $benefit) : ?>
					<div class="col-xl col-lg-4 col-sm-6 col-12 benefit-col wow flipInX" data-wow-delay="0.<?= $key * 2; ?>s">
						<div class="benefit-item">
							<div class="benefit-icon-wrap">
								<?php if ($benefit['ben_icon']) : ?>
									<img src="<?= $benefit['ben_icon']['url']; ?>" alt="benefit-icon">
								<?php endif; ?>
							</div>
							<h3 class="benefit-title"><?= $benefit['ben_title']; ?></h3>
							<p class="base-text benefit-text">
								<?= $benefit['ben_text']; ?>
							</p>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
