<?php

if(!isset($args['products']))
	return;

?>
<div class="product-slider-related arrows-slider">
	<?php foreach ($args['products'] as $_p){
		echo '<div class="slide-item p-2">';
		setup_postdata($GLOBALS['post'] =& $post);
		wc_get_template_part( 'content', 'product' );
		echo '</div>';
	}
	wp_reset_postdata();
	?>
</div>
