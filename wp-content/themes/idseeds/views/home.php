<?php
/*
Template Name: Homepage
*/

get_header();
$fields = get_fields();

?>

<?php if ($fields['main_slider_item']) : ?>
    <section class="main-block">
        <div class="main-slider">
            <?php foreach ($fields['main_slider_item'] as $slide) : $back_arr = isset($slide['background']) && $slide['background'] ? $slide['background'] : '';
			foreach ($back_arr as $back) : ?>
                <?php if ($back['acf_fc_layout'] === 'main_background_image') : ?>
                    <div>
                        <div class="main-slide-item"
                            <?php if ($slide_img = $back['image_url']) : ?>
                                style="background-image: url('<?= $slide_img['url']; ?>')"
                            <?php endif; ?>>
							<div class="content-part-slider">
								<div class="main-slider-text"><?= $slide['home_slider_content']; ?></div>
								<?php if ($slide['shop_now_link']) : ?>
									<a href="<?= $slide['shop_now_link']['url']; ?>" class="home-about-link">
										<?= (isset($slide['shop_now_link']['title']) && $slide['shop_now_link']['title'])
												? $slide['shop_now_link']['title'] : lang_text(['en' => 'shop now', 'es' => 'Compra ahora', 'ja' => '今すぐ購入'], 'en');
										?>
									</a>
								<?php endif; ?>
							</div>
                        </div>
                    </div>
                <?php endif;
                if ($back['acf_fc_layout'] === 'main_background_video') : ?>
                    <div>
                        <div class="main-slide-item main-video-item">
                            <?php if ($video = $back['video_file']) : ?>
                                <video muted autoplay="autoplay" loop="loop">
                                    <source src="<?= $video['url']; ?>" type="video/mp4">
                                </video>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach;endforeach; ?>
        </div>
    </section>
<?php endif;
if ($fields['first_slider_cats'] || $fields['seond_slider_cats']) : ?>
	<section class="categories-home">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-11">
					<div class="row justify-content-center">
						<?php if ($fields['first_slider_cats']) : ?>
							<div class="col-12 arrows-slider mb-2">
								<div class="cat-slider">
									<?php foreach ($fields['first_slider_cats'] as $i => $cat_item) :
										$thumbnail_id = get_term_meta($cat_item->term_id, 'thumbnail_id', true );
										$cat_image = wp_get_attachment_url( $thumbnail_id ); ?>
										<div class="slider-item-cat">
											<div class="home-category" <?php if ($cat_image) : ?> style="background-image: url('<?= $cat_image; ?>')"<?php endif; ?>>
												<a class="home-cat-overlay" href="<?= get_category_link($cat_item); ?>">
													<span class="home-cat-title"><?= $cat_item->name; ?></span>
												</a>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif;
						if ($fields['seond_slider_cats']) : ?>
							<div class="col-12 arrows-slider">
								<div class="cat-slider">
									<?php foreach ($fields['first_slider_cats'] as $i => $cat_item) :
										$thumbnail_id = get_term_meta($cat_item->term_id, 'thumbnail_id', true );
										$cat_image = wp_get_attachment_url( $thumbnail_id ); ?>
										<div class="slider-item-cat">
											<div class="home-category" <?php if ($cat_image) : ?> style="background-image: url('<?= $cat_image; ?>')"<?php endif; ?>>
												<a class="home-cat-overlay" href="<?= get_category_link($cat_item); ?>">
													<span class="home-cat-title"><?= $cat_item->name; ?></span>
												</a>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_prod_slider_1'] || $fields['h_prod_slider_2'] || $fields['h_prod_slider_3']) :
$sliders = [
		[
				'item' => $fields['h_prod_slider_1'],
				'title' => $fields['h_prod_slider_title_1'],
		],
		[
				'item' => $fields['h_prod_slider_2'],
				'title' => $fields['h_prod_slider_title_2'],
		],
		[
				'item' => $fields['h_prod_slider_3'],
				'title' => $fields['h_prod_slider_title_3'],
		],
]; ?>
	<section class="home-product-sliders">
		<div class="container">
			<div class="row justify-content-center">
				<?php foreach ($sliders as $slider) : if ($slider['item']) :  ?>
					<div class="col-xl-4 col-lg-6 col-12">
						<?php if ($slider['title']) : ?>
							<h3 class="home-slider-title"><?= $slider['title']; ?></h3>
						<?php endif;
						get_template_part('views/partials/content', 'product_slider', [
								'products' => $slider['item'],
							]); ?>
					</div>
				<?php endif; endforeach; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_about_text']) : ?>
	<section class="home-about-block mt-5">
		<div class="container">
			<div class="row justify-content-between align-items-start">
				<div class="<?= $fields['h_about_image'] ? 'col-lg-7 col-12' : 'col-12'; ?> d-flex flex-column justify-content-start align-items-center">
					<div class="base-output mb-3">
						<?= $fields['h_about_text']; ?>
					</div>
					<?php if ($fields['h_about_link']) : ?>
						<div class="align-self-end">
							<a href="<?= $fields['h_about_link']['url'];?>" class="home-about-link">
								<?= (isset($fields['h_about_link']['title']) && $fields['h_about_link']['title'])
										? $fields['h_about_link']['title'] : lang_text(['he' => 'קרא עוד עלינו', 'en' => 'Read more about us'], 'he');
								?>
							</a>
						</div>
					<?php endif; ?>
				</div>
				<?php if ($fields['h_about_image']) : ?>
					<div class="col-xl-4 col-lg-5 mt-lg-0 mt-4">
						<img src="<?= $fields['h_about_image']['url']; ?>" alt="about-image" class="home-about-img">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_benefits']) {
	get_template_part('views/partials/repeat', 'benefits', [
			'benefits' => $fields['h_benefits'],
	]);
}
if ($fields['banner_image'] || $fields['banner_text']) : ?>
	<section class="banner-block mb-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-11">
					<div class="banner-block-item">
						<img src="<?= IMG ?>banner-flowers.png" class="banner-flowers" alt="flowers">
						<div class="banner-inside row justify-content-end align-items-stretch">
							<?php if ($fields['banner_text'] || $fields['banner_link']) : ?>
								<div class="col-lg-7 banner-output-col">
									<img src="<?= IMG ?>banner-flowers.png" class="banner-flowers-small" alt="flowers">
									<div class="banner-output">
										<?= $fields['banner_text']; ?>
									</div>
									<?php if ($fields['banner_link']) : ?>
										<a href="<?= $fields['banner_link']['url'];?>" class="home-about-link">
											<?= (isset($fields['banner_link']['title']) && $fields['banner_link']['title'])
													? $fields['banner_link']['title'] : lang_text(['es' => 'Compra ahora', 'en' => 'shop now', 'ja' => '今すぐ購入'], 'en');
											?>
										</a>
									<?php endif; ?>
								</div>
							<?php endif;
							if ($fields['banner_image']) : ?>
								<div class="col-lg-4 col-12 banner-img" style="background-image: url('<?= $fields['banner_image']['url']; ?>')">
									<img src="<?= $fields['banner_image']['url']; ?>" alt="banner-image">
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['review_item']) : ?>
<section class="reviews-block mb-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<h2 class="base-title mb-4">
					<?= $fields['h_reviews_title'] ? $fields['h_reviews_title'] :
							lang_text(['en' => 'recommendation', 'es' => 'おすすめ', 'ja' => 'recomendación'], 'en'); ?>
				</h2>
			</div>
		</div>
		<div class="row justify-content-center align-items-stretch arrows-slider">
			<div class="col-sm-12 col-11">
				<div class="reviews-slider">
					<?php foreach ($fields['review_item'] as $x => $review) : ?>
						<div class="slider-review-item p-2">
							<div class="review-item">
								<h3 class="review-title">
									<?= $review['rev_name']; ?>
								</h3>
								<div class="base-output review-output">
									<?= $review['rev_text']; ?>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif;
get_footer(); ?>
