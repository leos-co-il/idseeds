<?php
/*
Template Name: Blog
*/

get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => 8,
	'post_type' => 'post',
	'suppress_filters' => false
]);
$published_posts = '';
$count_posts = wp_count_posts('post');
if ( $count_posts ) {
	$published_posts = $count_posts->publish;
}
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col">
				<h2 class="base-title text-center"><?php the_title(); ?></h2>
			</div>
		</div>
		<div class="row justify-content-center mb-4">
			<div class="col-auto">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts && $published_posts > 8) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="block-link more-link load-more-posts">
						<?= lang_text(['ja' => 'もっと読み込む', 'es' => 'Carga más', 'en' => 'Load more...'], 'en')?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq', [
		'faq' => $fields['faq_item'],
		'title' => $fields['faq_title'],
	]);
}
get_footer(); ?>
