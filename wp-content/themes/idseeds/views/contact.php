<?php
/*
Template Name: Contact
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$open_hours = opt('open_hours');
?>

<article class="page-body contact-page pt-4">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-11">
				<div class="row justify-content-center align-items-stretch">
					<div class="col col-contact-wrapper">
						<h2 class="base-title mb-3"><?php the_title(); ?></h2>
						<div class="base-output">
							<?php the_content(); ?>
						</div>
						<div class="row justify-content-between">
							<?php if ($tel) : ?>
								<div class="col-sm-6 col-11 contact-item contact-item-link wow zoomIn"
									 data-wow-delay="0.2s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-tel.png">
									</div>
									<div class="contact-item-col">
										<h3 class="contact-title-item">
											<?= lang_text(['en' => 'tel:', 'es' => 'tel:', 'ja' => '電話:'], 'en')?>
										</h3>
										<a href="tel:<?= $tel; ?>" class="contact-info">
											<?= $tel; ?>
										</a>
									</div>
								</div>
							<?php endif;
							if ($mail) : ?>
								<div class="contact-item col-sm-6 col-11 contact-item-link wow zoomIn"
									 data-wow-delay="0.4s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-mail.png">
									</div>
									<div class="contact-item-col">
										<h3 class="contact-title-item">
											<?= lang_text(['en' => 'mail:', 'es' => 'correo:', 'ja' => '郵便物:'], 'en')?>
										</h3>
										<a href="mailto:<?= $mail; ?>" class="contact-info">
											<?= $mail; ?>
										</a>
									</div>
								</div>
							<?php endif;
							if ($open_hours) : ?>
								<div class="contact-item-link contact-item col-sm-6 col-11 wow zoomIn" data-wow-delay="0.8s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-hours.png">
									</div>
									<div class="contact-item-col">
										<h3 class="contact-title-item">
											<?= lang_text(['en' => 'Activity time:', 'es' => 'Tiempo de actividad:', 'ja' => '活動時間:'], 'en')?>
										</h3>
										<div class="contact-info">
											<?= $open_hours; ?>
										</div>
									</div>
								</div>
							<?php endif;
							if ($address) : ?>
								<div class="contact-item-link contact-item col-sm-6 col-11 wow zoomIn" data-wow-delay="0.6s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-geo.png">
									</div>
									<div class="contact-item-col">
										<h3 class="contact-title-item">
											<?= lang_text(['en' => 'address:', 'es' => 'dirección:', 'ja' => '住所'], 'en')?>
										</h3>
										<a class="contact-info" href="https://www.waze.com/ul?q=<?= $address; ?>">
											<?= $address; ?>
										</a>
									</div>
								</div>
							<?php endif; ?>
						</div>
						<?php if ($map = opt('map_image')) : ?>
							<div class="contact-map-wrap">
								<img src="<?= $map['url']; ?>" alt="map">
							</div>
						<?php endif; ?>
					</div>
					<div class="col-xl-5 col-12">
						<div class="contact-form-wrap">
							<?php if ($fields['contact_form_title']) : ?>
								<h2 class="contact-form-title">
									<?= $fields['contact_form_title']; ?>
								</h2>
							<?php endif;
							if ($fields['contact_form_subtitle']) : ?>
								<h2 class="contact-form-subtitle">
									<?= $fields['contact_form_subtitle']; ?>
								</h2>
							<?php endif;
							getForm('64'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
