<?php

add_role( 'leos_client', __(

    'Leos Client' ),

    array(
        'activate_plugins' => false,
        'delete_others_pages' => true,
        'delete_others_posts' => true,
        'delete_pages' => true,
        'delete_posts' => true,
        'delete_private_pages' => true,
        'delete_private_posts' => true,
        'delete_published_pages' => true,
        'delete_published_posts' => true,
        'edit_dashboard' => true,
        'edit_others_pages' => true,
        'edit_others_posts' => true,
        'edit_pages' => true,
        'edit_posts' => true,
        'edit_private_pages' => true,
        'edit_private_posts' => true,
        'edit_published_pages' => true,
        'edit_published_posts' => true,
        'edit_theme_options' => true,
        'export' => false,
        'import' => false,
        'list_users' => true,
        'manage_categories' => true,
        'manage_links' => true,
        'manage_options' => true,
        'moderate_comments' => true,
        'promote_users' => false,
        'publish_pages' => true,
        'publish_posts' => true,
        'read_private_pages' => true,
        'read_private_posts' => true,
        'read' => true,
        'remove_users' => false,
        'switch_themes' => false,
        'upload_files' => true,
        'update_core' => false,
        'update_plugins' => false,
        'update_themes' => false,
        'install_plugins' => false,
        'install_themes' => false,
        'delete_themes' => false,
        'delete_plugins' => false,
        'edit_plugins' => false,
        'edit_themes' => false,
        'edit_files' => false,
        'edit_users' => false,
        'add_users' => false,
        'create_users' => false,
        'delete_users' => false,
        'unfiltered_html' => true,
        'manage_woocommerce' => true,
        'manage_woocommerce_orders'	=> true,
        'manage_woocommerce_coupons' => true,
        'manage_woocommerce_products' => true,
        'edit_products' => true,
        'edit_others_products' => true,
        'copy_posts' => true,
        'view_woocommerce_reports' => true,
        'edit_product' => true,
        'read_product' => true,
        'delete_product' => true,
        'publish_products' => true,
        'read_private_products' => true,
        'delete_products' => true,
        'delete_private_products' => true,
        'delete_published_products' => true,
        'delete_others_products' => true,
        'edit_private_products' => true,
        'edit_published_products' => true,
        'manage_product_terms' => true,
        'edit_product_terms' => true,
        'delete_product_terms' => true,
        'assign_product_terms' => true,
        'edit_shop_order' => true,
        'read_shop_order' => true,
        'delete_shop_order' => true,
        'edit_shop_orders' => true,
        'edit_others_shop_orders' => true,
        'publish_shop_orders' => true,
        'read_private_shop_orders' => true,
        'delete_shop_orders' => true,
        'delete_private_shop_orders' => true,
        'delete_published_shop_orders' => true,
        'delete_others_shop_orders' => true,
        'edit_private_shop_orders' => true,
        'edit_published_shop_orders' => true,
        'manage_shop_order_terms' => true,
        'edit_shop_order_terms' => true,
        'delete_shop_order_terms' => true,
        'assign_shop_order_terms' => true,
        'edit_shop_coupon' => true,
        'read_shop_coupon' => true,
        'delete_shop_coupon' => true,
        'edit_shop_coupons' => true,
        'edit_others_shop_coupons' => true,
        'publish_shop_coupons' => true,
        'read_private_shop_coupons' => true,
        'delete_shop_coupons' => true,
        'delete_private_shop_coupons' => true,
        'delete_published_shop_coupons' => true,
        'delete_others_shop_coupons' => true,
        'edit_private_shop_coupons' => true,
        'edit_published_shop_coupons' => true,
        'manage_shop_coupon_terms' => true,
        'edit_shop_coupon_terms' => true,
        'delete_shop_coupon_terms' => true,
        'assign_shop_coupon_terms' => true,
        'edit_shop_webhook' => true,
        'read_shop_webhook' => true,
        'delete_shop_webhook' => true,
        'edit_shop_webhooks' => true,
        'edit_others_shop_webhooks' => true,
        'publish_shop_webhooks' => true,
        'read_private_shop_webhooks' => true,
        'delete_shop_webhooks' => true,
        'delete_private_shop_webhooks' => true,
        'delete_published_shop_webhooks' => true,
        'delete_others_shop_webhooks' => true,
        'edit_private_shop_webhooks' => true,
        'edit_published_shop_webhooks' => true,
        'manage_shop_webhook_terms' => true,
        'edit_shop_webhook_terms' => true,
        'delete_shop_webhook_terms' => true,
        'assign_shop_webhook_terms' => true,
    )
);

function get_current_user_role($type = 'role') {
    global $wp_roles;
    $current_user = wp_get_current_user();
    if ($type == 'role') {
        $roles = $current_user->roles;
        $role = array_shift($roles);
        return isset($wp_roles->role_names[$role]) ? translate_user_role($wp_roles->role_names[$role] ) : false;
    } elseif ($type == 'id') {
        $roles = $current_user->ID;
        return $roles;
    } elseif ($type == 'mail') {
        $roles = $current_user->data->user_email;
        return $roles;
    }
}

function remove_admin_menu_links(){
    $user = get_current_user_role();
    if( $user == 'Leos Client' ) {
        remove_menu_page('tools.php');
        remove_menu_page('plugins.php');
        remove_menu_page('options-general.php');
        remove_menu_page('edit.php?post_type=fancy-gallery');
        remove_menu_page('edit.php?post_type=gallery');
        remove_menu_page('edit.php?post_type=acf-field-group');
        remove_menu_page('leos_settings');
        remove_menu_page('wpcf7' );
        remove_menu_page('itsec');
        remove_menu_page('themes.php');
        remove_submenu_page( 'profile.php', 'user-new.php' );
        remove_submenu_page( 'index.php', 'update-core.php' );
        remove_menu_page('sitepress-multilingual-cms/menu/languages.php');
        add_menu_page( 'menus', 'תפריטים', 'manage_options', 'nav-menus.php', '', 'dashicons-menu', 30 );
    } else {
        remove_submenu_page( 'themes.php', 'nav-menus.php' );
        remove_submenu_page( 'themes.php', 'widgets.php' );
        remove_submenu_page( 'themes.php', 'customize.php' );
        remove_menu_page('edit.php?post_type=fancy-gallery');
        add_menu_page( 'menus', 'Menus', 'manage_options', 'nav-menus.php', '', 'dashicons-menu', 30 );
        add_menu_page( 'menus', "Weejet", 'manage_options', 'widgets.php', '', 'dashicons-welcome-widgets-menus', 31 );
    }
}
add_action('admin_menu', 'remove_admin_menu_links', 9999);

remove_theme_support( 'genesis-admin-menu' );

function remove_customize_page(){
    global $submenu;
    unset($submenu['themes.php'][6]);
}
add_action( 'admin_menu', 'remove_customize_page');

add_filter('acf/settings/show_admin', 'my_acf_show_admin');

function my_acf_show_admin( $show ) {
    if( get_current_user_role() == 'Leos Client' ) {
        $can = 0;
    } else {
        $can = 1;
    }
    return $can;
}
