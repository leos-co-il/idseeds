<?php

if ( defined( 'YITH_WCWL' ) ) {
	function yith_wcwl_ajax_update_count() {
		wp_send_json( array(
			'count' => yith_wcwl_count_all_products()
		) );

	}
	add_action( 'wp_ajax_yith_wcwl_update_wishlist_count', 'yith_wcwl_ajax_update_count' );
	add_action( 'wp_ajax_nopriv_yith_wcwl_update_wishlist_count', 'yith_wcwl_ajax_update_count' );
}


function remove_item_from_cart(){

	$key = isset($_REQUEST['product_id']) ? $_REQUEST['product_id'] : null;

	if($key){
		WC()->cart->remove_cart_item($key);
	}

	wc_get_template( 'cart/mini-cart.php' );
	die();
}


add_action('wp_ajax_remove_item_from_cart', 'remove_item_from_cart');
add_action('wp_ajax_nopriv_remove_item_from_cart', 'remove_item_from_cart');

function add_product_to_cart() {

	$product_id = isset($_REQUEST['product_id']) ?  intval($_REQUEST['product_id']) : null;
	$quantity = isset($_REQUEST['quantity']) ? intval($_REQUEST['quantity']) : null;
	$custom_data = [];
	$_product = wc_get_product( $product_id );


	$cart = WC()->cart;

	if($_POST['delete'] === 'true'){
		$cart->remove_cart_item($product_id);
	}else{
		$cart->add_to_cart( $product_id, $quantity, '0', array(), $custom_data );
	}

	$result['type'] = "success";

	wc_get_template( 'cart/mini-cart.php' );

	die();
}

add_action('wp_ajax_add_product_to_cart', 'add_product_to_cart');
add_action('wp_ajax_nopriv_add_product_to_cart', 'add_product_to_cart');


add_action('wp_ajax_nopriv_update_cart_count', 'update_cart_count');
add_action('wp_ajax_update_cart_count', 'update_cart_count');
function update_cart_count(){
	global $woocommerce;
	echo $woocommerce->cart->cart_contents_count;
}


add_action('wp_ajax_nopriv_update_mini_cart', 'update_mini_cart');
add_action('wp_ajax_update_mini_cart', 'update_mini_cart');
function update_mini_cart(){
	$id = filter_var($_POST['id'], FILTER_SANITIZE_STRING);
	$qty = intval($_POST['qty']);

	global $woocommerce;
	$woocommerce->cart->set_quantity($id,$qty);


	$result['type'] = "success";

	wc_get_template( 'cart/mini-cart.php' );

	die();
}

add_action('wp_ajax_nopriv_get_more_function', 'get_more_function');
add_action('wp_ajax_get_more_function', 'get_more_function');

function get_more_function()
{
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$type = (isset($_REQUEST['type'])) ? $_REQUEST['type'] : 'post';
	$count_cat = (isset($_REQUEST['count'])) ? $_REQUEST['count'] : '';
	$id_term = (isset($_REQUEST['termID'])) ? $_REQUEST['termID'] : '';
	$id_page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : '';
	$quantity = (isset($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : '';
	$ids = explode(',', $ids_string);
	$cat = ($type === 'project') ? 'project_cat' : 'category';
	$html = '';
	$result['html'] = '';
	$result['quantity'] = '';
	if ($type === 'post') {
		$query = new WP_Query([
			'post_type' => $type,
			'posts_per_page' => 4,
			'post__not_in' => $ids,
			'tax_query' => $id_term ? [
				[
					'taxonomy' => $cat,
					'field' => 'term_id',
					'terms' => $id_term,
				]
			] : '',
		]);
		if ($query->have_posts()) {
			foreach ($query->posts as $item) {
				$html = load_template_part('views/partials/card', 'post', [
					'post' => $item,
				]);
				$result['html'] .= $html;
			}
			if (wp_count_posts($type)->publish <= ($quantity + 4)) {
				$result['quantity'] = true;
			}
		}
	}
	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}

add_action( 'wp_ajax_loadmore', 'loadmore' );
add_action( 'wp_ajax_nopriv_loadmore', 'loadmore' );
function loadmore() {
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$ids = explode(',', $ids_string);
	$paged = ! empty( $_POST[ 'paged' ] ) ? $_POST[ 'paged' ] : 1;
	$paged++;

	$args = array(
		'paged' => $paged,
		'post_type' => 'product',
		'post_status' => 'publish',
		'post__not_in' => $ids,
	);
	$query = new WP_Query( $args );
	$html = '';
	$result['html'] = '';
	foreach( $query->posts as $item) :
		$html = load_template_part('views/partials/card', 'product_col', [
			'product' => $item,
		]);
		$result['html'] .= $html;
	endforeach;
	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();

}
