<?php

the_post();
get_header();
?>
<section class="page-body">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="base-output">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
