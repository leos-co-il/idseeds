<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body mb-5">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container mb-5">
		<div class="row justify-content-between">
			<div class="<?= has_post_thumbnail() ? 'col-lg-8 col-12' : 'col-12'; ?>">
				<h1 class="base-title"><?php the_title(); ?></h1>
				<div class="base-output post-output">
					<?php the_content(); ?>
				</div>
				<div class="socials-share">
					<span class="share-text">
						 <?= lang_text(['es' => 'cuota', 'en' => 'share', 'ja' => '共有'], 'en'); ?>
					</span>
					<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.6s">
						<img src="<?= ICONS ?>share-mail.png">
					</a>
					<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.4s">
						<img src="<?= ICONS ?>share-whatsapp.png">
					</a>
					<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.2s">
						<img src="<?= ICONS ?>share-facebook.png">
					</a>
				</div>
			</div>
			<?php if (has_post_thumbnail()) : ?>
				<div class="col-lg-4 col-12 page-form-col-post">
					<img src="<?= postThumb(); ?>" alt="post-image" class="post-page-img w-100">
				</div>
			<?php endif; ?>
		</div>
		<?php if ($fields['post_additional_content']) : ?>
			<div class="row">
				<div class="col">
					<div class="base-output">
						<?= $fields['post_additional_content']; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} else {
	$samePosts = get_posts([
			'posts_per_page' => 4,
			'post_type' => 'post',
			'post__not_in' => array($postId),
			'tax_query' => [
					[
							'taxonomy' => 'category',
							'field' => 'term_id',
							'terms' => $post_terms,
					],
			],
	]);
}
if ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 4,
			'orderby' => 'rand',
			'post_type' => 'post',
			'post__not_in' => array($postId),
	]);
} if ($samePosts) : ?>
<section class="same-posts-output">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<h2 class="base-title text-center mb-4">
					<?= lang_text(['en' => 'more articles', 'es' => 'más artículos', 'ja' => 'その他の記事'], 'en'); ?>
				</h2>
			</div>
		</div>
		<div class="row justify-content-center align-items-stretch">
			<?php foreach ($samePosts as $post) {
				get_template_part('views/partials/card', 'post',
						[
								'post' => $post,
						]);
			} ?>
		</div>
	</div>
</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
