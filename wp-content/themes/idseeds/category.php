<?php
get_header();
$query = get_queried_object();
$posts = new WP_Query([
	'posts_per_page' => 8,
	'post_type' => 'post',
	'suppress_filters' => false
]);
$published_posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'post',
	'suppress_filters' => false,
	'tax_query' => array(
		array(
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
$cats = get_terms([
	'taxonomy' => 'category',
	'hide_empty' => false,
	'exclude' => $query->term_id,
]);
$img = get_field('top_img', $query);
?>

<article class="page-body p-block mb-5">
	<?php get_template_part('views/partials/content', 'top_page',
		[
			'img' => $img ? $img['url'] : '',
			'title' => $query->name,
		]);
	if ($cats) : ?>
		<div class="container">
			<div class="row justify-content-center align-items-stretch mb-2">
				<?php foreach ($cats as $cat_item) : ?>
					<div class="col-auto d-flex justify-content-center align-items-center cat-link-col">
						<a href="<?= get_term_link($cat_item); ?>" class="cat-link">
							<?= $cat_item->name; ?>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-center mb-4">
			<div class="col-xl-8 col-lg-9 col-md-10 col-11">
				<div class="base-output text-center">
					<?= category_description(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts->have_posts() && $published_posts->posts > 8) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="block-link more-link load-more-posts" data-type="post" data-term="<?= $query->term_id; ?>">
						<?= lang_text(['he' => 'טען עוד..', 'en' => 'Load more...'], 'he')?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php if ($seo = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $seo,
			'img' => get_field('slider_img', $query),
	]);
} if ($faq = get_footer('faq_item', $query)) {
	get_template_part('views/partials/content', 'slider', [
			'faq' => $faq,
			'title' => get_field('faq_title', $query),
	]);
}
get_footer(); ?>
